const quizData = [
    {
        question: "Binatang apa yang jago renang?",
        a: "Ikan",
        b: "Kucing",
        c: "Bebek",
        d: "Kerbau",
        correct: "c",
    },
    {
        question: "Kenapa di rel kereta api ditaruh batu?",
        a: "Karna kalau ditaruh uang, ya diambil orang",
        b: "Biar ga miring",
        c: "Biar ga terbang",
        d: "Semau salah",
        correct: "a",
    },
    {
        question: "Ayam apa yang bikin sebel?",
        a: "Ayam eek di kasur",
        b: "Ayam bakar",
        c: "Ayamnya habis, tapi nasinya masih",
        d: "Ayamku dimakan adek",
        correct: "c",
    },
    {
        question: "Kenapa di komputer ada tulisan ENTER?",
        a: "Karena kalau tulisannya ENTAR, ga jalan2 nanti",
        b: "Tanya aja sama komputernya",
        c: "Karena biar yoi",
        d: "Biar menambah keunikan",
        correct: "a",
    },
    {
        question: "Kunci apa yang bikin orang joget??",
        a: "KUNCI KUNCI KO TAHE",
        b: "KUNCITA DIA",
        c: "KUCINTAI KAMU",
        d: "KUCINTAI MEREKA",
        correct: "a",
    }
];

const quiz = document.getElementById("quiz");
const answerEls = document.querySelectorAll(".answer");
const questionEl = document.getElementById("question");
const a_text = document.getElementById("a_text");
const b_text = document.getElementById("b_text");
const c_text = document.getElementById("c_text");
const d_text = document.getElementById("d_text");
const submitBtn = document.getElementById("submit");


let currentQuiz = 0;
let score = 0;
const username = "ricogann";
const password = "12345678";

function login(){
    const inputUsername = document.getElementById("username").value;
    const inputPassword = document.getElementById("password").value;

    if(username===inputUsername && password===inputPassword){
        window.location.href="quiz.html";
    }else{
        alert("Usernamenya ricogann dan Passwordnya 12345678")
    }
}

loadQuiz();

function loadQuiz() {
    deselectAnswers();

    const currentQuizData = quizData[currentQuiz];

    questionEl.innerText = currentQuizData.question;
    a_text.innerText = currentQuizData.a;
    b_text.innerText = currentQuizData.b;
    c_text.innerText = currentQuizData.c;
    d_text.innerText = currentQuizData.d;
}

function getSelected() {
    let answer = undefined;

    answerEls.forEach((answerEl) => {
        if (answerEl.checked) {
            answer = answerEl.id;
        }
    });

    return answer;
}

function deselectAnswers() {
    answerEls.forEach((answerEl) => {
        answerEl.checked = false;
    });
}

submitBtn.addEventListener("click", () => {
    const answer = getSelected();
    console.log(answer);
    if (answer) {
        if (answer === quizData[currentQuiz].correct) {
            score++;
        }

        currentQuiz++;
        if (currentQuiz < quizData.length) {
            loadQuiz();
        } else {
            quiz.innerHTML = `
                <h1>You answered correctly at ${score}/${quizData.length} questions.</h1>
                <div class="button-chc">
                <button onclick="location.reload()" class="btn-rld">Reload</button>
                <button onclick="logout()" class="btn-rld">Logout</button>
                </div>
            `;
        }
    }
});

function logout(){
    window.location.href="index.html";
}

function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

window.onclick = function(event) {
    if (!event.target.matches('.dropbtn')) {
      var dropdowns = document.getElementsByClassName("dropdown-content");
      var i;
      for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show');
        }
      }
    }
  }